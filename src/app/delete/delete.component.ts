import { Component, OnInit } from "@angular/core";
import { AuthService } from "../_service/auth.service";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { User } from "../_service/user";

@Component({
  selector: "app-delete",
  templateUrl: "./delete.component.html",
  styleUrls: ["./delete.component.css"]
})
export class DeleteComponent implements OnInit {
  userId: string = "";
  ID: number;
  confirmDelete: boolean = false;
  done: boolean = false;

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit() {}

  getUserId(event: Event) {
    this.userId = (<HTMLInputElement>event.target).value;
    this.ID = Number(this.userId);
    console.log(this.userId);
    console.log(this.ID);
  }

  passID() {
    this.delete(this.ID);
  }

  permissionLogout() {
    this.authService.logout()
  }

  delete(ID: number) {
    this.authService
      .delete(ID)
      .then(response => {
        console.log("response: ", response);
      })
      .catch(err => {
        console.log(err);
      });
    this.done = true;

    this.confirmDelete = true;
  }

  backToAdmin() {
    this.router.navigate(["/admin"]);
  }
}
