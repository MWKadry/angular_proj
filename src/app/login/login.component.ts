import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_service/auth.service';
import { HttpClient } from '@angular/common/http';
import { RouterModule, Router } from '@angular/router';
import { User } from '../_service/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  name: string='';
  pass: string='';
  allowSubmit: boolean= false;

  constructor(private authService: AuthService, private http: HttpClient, private router: Router) {}

  ngOnInit() {

  }

  onSubmit(userInfo: {userName: string, password: string }) {
    this.authService.login(userInfo).then( (response) => {
      console.log('response: ', response);
      let user: User= response;
      if(user.userRole=='admin'){
        this.router.navigate(['/admin']);
      }else{
        this.router.navigate(['/user']);
      }
    }).catch((err) => {
      console.log(err);
    });
    
  }

  onUpdateUserName(event: Event) {
    this.name = (<HTMLInputElement>event.target).value;
    this.empty(this.name, this.pass);
    event.preventDefault();
    console.log(this.name);
  }

  onUpdatePassword(event: Event) {
    this.pass = (<HTMLInputElement>event.target).value;
    this.empty(this.name, this.pass);
    event.preventDefault();
    console.log(this.pass);
  }

  empty(name:string, pass:string) {
    if(name=='' || pass=='' || pass.length < 4) {
      this.allowSubmit= false;
    }else {
      this.allowSubmit= true;
    }
  }

}
