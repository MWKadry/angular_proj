import { Component, OnInit } from "@angular/core";
import { AuthService } from "../_service/auth.service";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { User } from "../_service/user";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"]
})
export class UserComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit() {}

  permissionLogout() {
    this.authService.logout();
  }
}
