import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { RouterModule, Router } from '@angular/router';
import { User } from "./user";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  login(userInfo) {
    let headers = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Basic " + btoa("admin:admin")
      })
    };

    let promise = new Promise((resolve, reject) => {
      this.http
        .post<User>("/api/user/login", userInfo, httpOptions)
        .toPromise()
        .then(
          res => {
            localStorage.setItem(
              "Auth",
              btoa(userInfo.userName + ":" + userInfo.password)
            );
            localStorage.setItem("currentUser", userInfo.userName);
            resolve(res);
          },
          msg => {
            reject(msg);
          }
        );
    });
    return promise;
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("currentUser");
    this.router.navigate(['']);
  }

  add(newUser) {
    let headers = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Basic " + localStorage.getItem("Auth")
      })
    };

    let promise = new Promise((resolve, reject) => {
      this.http
        .post<User>("/api/user", newUser, httpOptions)
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          msg => {
            reject(msg);
          }
        );
    });
    return promise;
  }

  edit(updatedUser) {
    let headers = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Basic " + localStorage.getItem("Auth")
      })
    };

    let promise = new Promise((resolve, reject) => {
      this.http
        .put<User>("/api/user/update", updatedUser, httpOptions)
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          msg => {
            reject(msg);
          }
        );
    });
    return promise;
  }

  delete(ID: number) {
    let headers = new HttpHeaders();
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Basic " + localStorage.getItem("Auth")
      })
    };

    let promise = new Promise((resolve, reject) => {
      this.http
        .delete("/api/user/" + ID + "/delete", httpOptions)
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          msg => {
            reject(msg);
          }
        );
    });
    return promise;
  }

  getUsers() {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Basic " + localStorage.getItem("Auth")
      })
    };

    let promise = new Promise((resolve, reject) => {
      this.http
        .get("/api/user/all", httpOptions)
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          msg => {
            reject(msg);
          }
        );
    });
    return promise;
  }
}
