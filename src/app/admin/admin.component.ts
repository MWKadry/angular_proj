import { Component, OnInit } from "@angular/core";
import { AuthService } from "../_service/auth.service";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { User } from "../_service/user";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"]
})
export class AdminComponent implements OnInit {
  emp: User;
  display: boolean = false;
  deleteForm: boolean = false;
  addForm: boolean= false;
  userId: any;
  confirmAdd: boolean= false; 

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit() {}

  add(newUser: {
    userName: string;
    password: string;
    phoneNumber: number;
    address: string;
    email: string;
    userRole: string;
    famlyName: string;
  }) {
    
    this.addForm = false;
    this.authService
      .add(newUser)
      .then(response => {
        console.log("response: ", response);
      })
      .catch(err => {
        console.log(err);
      });
      this.confirmAdd= true;
  }

  closeConfirm() {
    this.confirmAdd= false;
  }

  toggleGet(){
    this.confirmAdd= false;
    this.addForm= false;
    this.getAll();

    if(this.display!=true){
      this.display = true;
    }else {
      this.display = false;
    }
  }
  

  permissionLogout() {
    this.authService.logout()
  }

  getAll() {
    this.authService
      .getUsers()
      .then(response => {
        console.log("response: ", response);
        this.emp = response;
      })
      .catch(err => {
        console.log(err);
      });
  }

  // delete(userId) {
  //   this.authService
  //     .delete(userId)
  //     .then(response => {
  //       console.log("response: ", response);
  //     })
  //     .catch(err => {
  //       console.log(err);
  //     });
  // }

  editUser(updatedUser: {
    userName: string;
    password: string;
    phoneNumber: number;
    address: string;
    email: string;
    userRole: string;
    famlyName: string;
  }) {
    this.authService
      .edit(updatedUser)
      .then(response => {
        console.log("response: ", response);
      })
      .catch(err => {
        console.log(err);
      });
  }

  openAddForm() {
    this.addForm = true;
  }

  openDeleteForm() {
    this.router.navigate(['/delete']);
  }
}
