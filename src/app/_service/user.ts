export class User {
  address?: string;
  email?: string;
  familyName?: string;
  password?: string;
  phoneNumber?: number;
  userDeleted?: number;
  userName?: string;
  userRole?: string;
  userId?: number;
}
